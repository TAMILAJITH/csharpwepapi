using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using Newtonsoft.Json.Serialization;
using System.Linq;
using System.Collections.Generic;





namespace test.Controllers
{

    [Route("api/value")]
    [ApiController]

    public class valueController : ControllerBase
    {  


        ProductService Product;
        IConfiguration Configure;                            

        public valueController(ProductService productService, IConfiguration Configuration)
        {
            this.Product = productService;
            this.Configure = Configuration;

        }

        [HttpGet]

        public ActionResult Get()
        {
            // return Ok(this.Product.getProduct());

            
            string query = @"select * from UserData";
            DataTable table = new DataTable();
            string sqlDatasource = Configure.GetConnectionString("UserData");
            SqlDataReader dataReader;
            using (SqlConnection myCon = new SqlConnection(sqlDatasource))
            {
                myCon.Open();
                using (SqlCommand cmd = new SqlCommand(query, myCon))
                {
                    dataReader = cmd.ExecuteReader();
                    table.Load(dataReader);
                    myCon.Close();
                    dataReader.Close();
                }
            }


            return new JsonResult(table);

        }


        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            return Ok(this.Product.getProductByRecId(id));
        }


        [HttpPost]

        public ActionResult post(ProductModel product)
        {
            // this.Product.setProduct(product);

            string query = @"insert into UserData values ('" + product.Name + @"','" + product.Age + @"','" + product.Color + @"','" + product.Status + @"')";
            string dataSource = Configure.GetConnectionString("UserData");
            SqlDataReader dataReader;
            DataTable table = new DataTable();
            using (SqlConnection connection = new SqlConnection(dataSource))
            {
                connection.Open();                                  
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    dataReader = command.ExecuteReader();   
                    table.Load(dataReader);         
                }
            }
            return new JsonResult("Added Successfully");
        }

        [HttpPut]

        public ActionResult put(int RecId, [FromBody] ProductModel product)
        {

            // this.Product.updateProduct(id, product);
            string query = @"update UserData set Name='" + product.Name + @"',Age='" + product.Age + @"',Color='" + product.Color + @"',Status='" + product.Status + @"' where RecId ='"+RecId+"'";
            DataTable table = new DataTable();
            SqlDataReader dataReader;
            string serverPath = Configure.GetConnectionString("UserData");
            using (SqlConnection connection = new SqlConnection(serverPath))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand(query, connection))
                {
                    dataReader = command.ExecuteReader();
                    table.Load(dataReader);
                }
            }
            return new JsonResult("Updated Successfully");

        }

        [HttpDelete]
        public ActionResult delete(int RecId)
        {
            // this.Product.deleteProduct(id);

            string query =@"delete from UserData where RecId='"+RecId+@"'";
            DataTable table =new DataTable();
            SqlDataReader dataReader;
            string serverPath =Configure.GetConnectionString("UserData");
            using(SqlConnection connection =new SqlConnection(serverPath)){
                connection.Open();
                using(SqlCommand command =new SqlCommand(query,connection)){
                    dataReader =command.ExecuteReader();
                    table.Load(dataReader);
                }
            }
            return new JsonResult("Deleted Successfully");
        }
    }
}