using System;
using System.Collections.Generic;


namespace test.Controllers
{

    public class ProductService
    {

        List<ProductModel> _product = null;
        public ProductService()
        {
            _product = new List<ProductModel>();
        }

        public List<ProductModel> getProduct()
        {
           
            return _product;
        }

        public  ProductModel getProductByRecId(int id){
            return _product[id];
        }

        public void setProduct (ProductModel product){
            _product.Add(product);
        }

        public void updateProduct(int id,ProductModel product){
            _product[id]=product;
        }

        public void deleteProduct (int id){
            _product.RemoveAt(id);
        }

    }
}