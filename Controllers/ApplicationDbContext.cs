using System;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;

namespace test.Controllers      
{
    public class ApplicationDbContext : IdentityDbContext
    {
            
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options){

        }

        public DbSet<ProductModel> Data {get;set;}

    }
}