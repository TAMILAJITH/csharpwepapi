using System;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Data;

namespace test.Controllers
{

    [Route("api/data")]
    [ApiController]
    public class LinqQuery : ControllerBase
    {

        public ApplicationDbContext Context;

        public LinqQuery (ApplicationDbContext _Context){

       this.Context =_Context;

        }

      [HttpGet]
        public IActionResult get(){

            var query=from UserData in Context.Data select UserData;
            DataTable table =new DataTable();
            return new JsonResult(query);
        }


        
    }
}