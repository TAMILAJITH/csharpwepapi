using System;

namespace test.Controllers {
    public class ProductModel {
        public string Name {get;set;}
        public int Age {get;set;}
        public string Color {get;set;}
        public string Status {get;set;}
    }
}